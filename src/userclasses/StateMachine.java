/**
 * Your application code goes here
 */

package userclasses;

import generated.StateMachineBase;
import com.codename1.ui.*; 
import com.codename1.ui.events.*;
import com.codename1.ui.table.Table;
import com.codename1.ui.util.Resources;
import com.codename1.xml.Element;
import java.util.Vector;
import userclasses.util.modele.ModeleDeTableau;
import userclasses.util.parser.RequetePourArbreXML;
import static userclasses.util.parser.ParserArbreXML.getValeur;
import userclasses.util.parser.RequetePourTexteBrut;
/**
 *
 * @author Your name here
 */
public class StateMachine extends StateMachineBase {
    
       private RequetePourArbreXML req;
       private RequetePourTexteBrut  req2;
       private String num;
       private String str;
       private Element      racine;
       private Table tableau;
       private ModeleDeTableau model;
       
    public StateMachine(String resFile) {
        super(resFile);
        // do not modify, write code in initVars and initialize class members there,
        // the constructor might be invoked too late due to race conditions that might occur
    }

    @Override
    protected void postMain(Form f) {
        super.postMain(f);

    }

    @Override
    protected void onMain_ButtonAction(Component c, ActionEvent event) {
        super.onMain_ButtonAction(c, event);
        
        String email = this.findTextField().getText();
        String mdp = this.findTextField1().getText();
        
        req2 = new RequetePourTexteBrut();
        req2.executer("gc/client/"+email+"/"+mdp);
        
        num = req2.getTexte();
        
        if(!"".equals(num)){
            str ="Connexion : OK";
        }
        else str ="Connexion : Pas OK";
        
        Label lab ;
        lab = this.findLabel2();
        lab.setText(str);

        
        if("Connexion : OK".equals(str))
        {
               afficherTableau();
        }
        else
        {
               viderLeTableau();
        }
      
        
        
    }
    
    public void afficherTableau(){
        
            req = new RequetePourArbreXML();

            tableau = this.findTable();
            req.executer("gc/resumescommande/"+num+"");
            racine = req.getRacine();
            model = new ModeleDeTableau("numcom","datecom","desigprod","etatcom","nomcateg","prixprod");

            tableau.setModel(model);

            Vector<Element> lesCommandes = racine.getChildrenByTagName("dtoclient");

            for(Element com : lesCommandes){

                model.ajouterRangee(getValeur(com, "numcom"),getValeur(com, "datecom"),getValeur(com, "desigprod"),getValeur(com, "etatcom"),getValeur(com, "nomcateg"),getValeur(com, "prixprod"));

            }
            tableau.setModel(model);
    }
    
    public void viderLeTableau(){
        
            req = new RequetePourArbreXML();

            tableau = this.findTable();
            req.executer("gc/resumescommande/"+num+"");
            racine = req.getRacine();
            model = new ModeleDeTableau("numcom","datecom","desigprod","etatcom","nomcateg","prixprod");

            tableau.setModel(model);

            Vector<Element> lesCommandes = racine.getChildrenByTagName("dtoclient");

            for(Element com : lesCommandes){

                model.ajouterRangee(getValeur(com, "numcom"),getValeur(com, "datecom"),getValeur(com, "desigprod"),getValeur(com, "etatcom"),getValeur(com, "nomcateg"),getValeur(com, "prixprod"));

            }
            tableau.setModel(model);
            
            model.viderData();
            tableau.setModel(model);
    }
    
    

}
